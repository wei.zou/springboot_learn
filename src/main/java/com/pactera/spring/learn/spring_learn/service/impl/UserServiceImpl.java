package com.pactera.spring.learn.spring_learn.service.impl;

import com.fasterxml.jackson.databind.util.BeanUtil;
import com.pactera.spring.learn.spring_learn.common.MinioTemplate;
import com.pactera.spring.learn.spring_learn.exception.ServiceException;
import com.pactera.spring.learn.spring_learn.mapper.UserMapper;
import com.pactera.spring.learn.spring_learn.model.dto.UserDataDTO;
import com.pactera.spring.learn.spring_learn.model.entity.User;
import com.pactera.spring.learn.spring_learn.model.vo.UserDataVO;
import com.pactera.spring.learn.spring_learn.service.IUserService;
import io.minio.errors.MinioException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {
    @Resource
    private UserMapper userMapper;

    @Autowired
    private final MinioTemplate minioTemplate;

    public UserServiceImpl(MinioTemplate minioTemplate) {
        this.minioTemplate = minioTemplate;
    }

    @Override
    public Boolean updUser(UserDataDTO dto){
        User user = new User();
        BeanUtils.copyProperties(dto, user);
        return userMapper.updUser(user);
    }

    @Override
    public List<UserDataVO> getUserList(UserDataDTO dto){
        return userMapper.getUserList(dto);
    }

    @Override
    @Transactional
    public Boolean insertUser(UserDataDTO dto){
        // 全局拦截器
        if("admin".equals(dto.getName())){
            throw new ServiceException("不允许添加Admin用户");
        }

        User user = new User();
        BeanUtils.copyProperties(dto, user);
//        userMapper.insertUser(user);
//        userMapper.delUser(2);
//        return userMapper.insertUser(user) > 0;
        return userMapper.insertUser(user)>0;
    }

    @Override
    public UserDataVO getUserDetail(Integer id) {
        return userMapper.getUserDetail(id);
    }

    @Override
    @Transactional
    public Boolean delUser(Integer id) throws MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {
        UserDataVO userDataVO = userMapper.getUserDetail(id);
        String strAvatar = userDataVO.getAvatar();

        Boolean delResult = userMapper.delUser(id);
        if(delResult){
            minioTemplate.removeObject("strBucket", strAvatar);
        }
        return userMapper.delUser(id);
    }
}
