package com.pactera.spring.learn.spring_learn.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class LoginRestController {
    @GetMapping(value = "goToLoginRest")
    public String toLoginRest() {
        return "login.html";
    }
}