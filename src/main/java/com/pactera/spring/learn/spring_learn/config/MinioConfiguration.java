package com.pactera.spring.learn.spring_learn.config;

import com.pactera.spring.learn.spring_learn.properties.MinioProperties;
import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MinioConfiguration {

//    @Value("${minio.access-key}")
//    private String accessKey;
//    @Value("${minio.secret-key}")
//    private String secretKey;

    @Autowired
    private MinioProperties minioProperties;
    @Bean
    public MinioClient getMinioClient(){
        MinioClient minioClient = MinioClient.builder()
                .endpoint("http://127.0.0.1:9000")
                .credentials(minioProperties.getAccessKey(), minioProperties.getSecretKey())
                .build();

        return minioClient;
    }

}
