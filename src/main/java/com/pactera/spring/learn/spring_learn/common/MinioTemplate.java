package com.pactera.spring.learn.spring_learn.common;

import io.minio.*;
import io.minio.errors.MinioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@Component
public class MinioTemplate {
//    public  MinioTemplate(){
//        System.out.println("new MinioTemplate...");
//    }
    @Autowired
    private MinioClient minioClient;

    /**
     *
     * @param inputStream
     * @param contentType
     * @return
     * @throws MinioException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public String putObject(InputStream inputStream, String contentType) throws MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {

        // Make 'pactera' bucket if not exist.
        boolean found = minioClient.bucketExists(BucketExistsArgs.builder().bucket("pactera").build());
        if (!found) {
            // Make a new bucket called 'asiatrip'.
            minioClient.makeBucket(MakeBucketArgs.builder().bucket("pactera").build());
        } else {
            System.out.println("Bucket 'pactera' already exists.");
        }

        ObjectWriteResponse pacteraResponse = minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket("pactera")
                        .object(UUID.randomUUID().toString())
                        .contentType(contentType)
                        .stream(inputStream, -1, 6 * 1024 * 1024)
                        .build());

        return "http://127.0.0.1:9000/pactera/" + pacteraResponse.object();
    }

    /**
     *
     * @param bucket
     * @param objectName
     * @throws MinioException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    public void removeObject(String bucket, String objectName) throws MinioException, IOException, NoSuchAlgorithmException, InvalidKeyException {

        minioClient.removeObject(
                RemoveObjectArgs.builder()
                        .bucket(bucket)
                        .object(objectName).build());
    }
}
