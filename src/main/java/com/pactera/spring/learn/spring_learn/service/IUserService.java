package com.pactera.spring.learn.spring_learn.service;

import com.pactera.spring.learn.spring_learn.model.dto.UserDataDTO;
import com.pactera.spring.learn.spring_learn.model.vo.UserDataVO;

import java.util.List;

public interface IUserService {

    Boolean updUser(UserDataDTO dto);

    List<UserDataVO> getUserList(UserDataDTO dto);

    Boolean insertUser(UserDataDTO dto);

    UserDataVO getUserDetail(Integer id);

    Boolean delUser(Integer id) throws Exception;
}
