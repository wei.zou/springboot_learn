package com.pactera.spring.learn.spring_learn.mapper;

import com.pactera.spring.learn.spring_learn.model.dto.UserDataDTO;
import com.pactera.spring.learn.spring_learn.model.entity.User;
import com.pactera.spring.learn.spring_learn.model.vo.UserDataVO;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface UserMapper {

//    @Select("SELECT * FROM user WHERE name = #{dto.name}")
//    @Update("UPDATE user SET name = 'XX' WHERE ***")
//    @Delete("DELETE FROM ")
//    @Insert("INSERT INTO `user` (id, name, age, email, avatar) VALUE ()")

    /**
     * 获取用户列表
     * @param dto
     * @return
     */
    List<UserDataVO> getUserList(@Param("dto") UserDataDTO dto);

    /**
     * 通过id获取用户详情
     * @param id
     * @return
     */
    UserDataVO getUserDetail(@Param("id") Integer id);

    /**
     * 新增用户，单条
     * @param user
     * @return
     */
    int insertUser(@Param("user") User user);

    /**
     * 更新用户，单条
     * @param user
     * @return
     */
    Boolean updUser(@Param("user") User user);

    /**
     * 通过id删除用户
     * @param id
     * @return
     */
    Boolean delUser(@Param("id") Integer id);
}
