package com.pactera.spring.learn.spring_learn.controller;

import com.pactera.spring.learn.spring_learn.common.MinioTemplate;
import com.pactera.spring.learn.spring_learn.common.R;
import com.pactera.spring.learn.spring_learn.model.dto.UserDataDTO;
import com.pactera.spring.learn.spring_learn.service.IUserService;
import com.pactera.spring.learn.spring_learn.model.vo.UserDataVO;
import io.minio.errors.MinioException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@RequestMapping("/api")
public class UserController {
    //构造器注入（官方推荐）
    @Autowired
    private final IUserService userService;
    public UserController(IUserService userService){
        this.userService = userService;
        //System.out.println("new UserController...");
    }

    //字段注入
    @Autowired
    private MinioTemplate minioTemplate;

    //set注入（定义中没有final）
//    private IUserService userService;
//    @Autowired
//    public void setUserController(IUserService userService){
//        this.userService = userService;
//    }
    /**
     * 获取用户列表
     * @param dto 请求体
     * @return {@link UserDataVO}
     */
    @GetMapping("/user")
    public R<List<UserDataVO>>  getUserList(UserDataDTO dto) {
        System.out.println("Get user list!");
        return R.success(userService.getUserList(dto));
    }

    /**
     * 新增用户，单条
     * @param dto 请求体
     * @return
     */
    @PostMapping("/user")
    public Boolean insertUser(@RequestBody UserDataDTO dto) {
        return userService.insertUser(dto);
    }

    /**
     * √
     * @param dto
     * @return
     */
    @PutMapping("/user")
    public UserDataDTO updUser(@RequestBody UserDataDTO dto) {
        //TODO
        return dto;
    }

    /**
     * 通过id删除用户
     * @param id
     * @return
     */
    @DeleteMapping("/user/{id}")
    public Boolean delUser(@PathVariable Integer id) throws Exception {

        return userService.delUser(id);
    }

    /**
     * 通过id获取用户详情
     * @param id
     * @return
     */
    @GetMapping("/user/{id}")
    public UserDataVO getUserDetail(@PathVariable Integer id) {
        return userService.getUserDetail(id);
    }

    /**
     *
     * @param file
     * @return
     * @throws IOException
     * @throws MinioException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
    @PostMapping("uploadAvatar")
    public R<String> uploadAvatar(@RequestParam("file")MultipartFile file) throws IOException, MinioException, NoSuchAlgorithmException, InvalidKeyException {
        return R.success(minioTemplate.putObject(file.getInputStream(), file.getContentType()));
    }
}