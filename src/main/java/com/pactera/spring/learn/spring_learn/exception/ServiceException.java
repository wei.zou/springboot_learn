package com.pactera.spring.learn.spring_learn.exception;

public class ServiceException extends RuntimeException{
    /**
     * 自定义异常，应该在serviceImpl层抛出
     * @param message
     */
    public ServiceException(String message) {
        super(message);
    }
}
